import java.util.ArrayList;

// A Binary Tree node
class Node {
	int id;
    ArrayList<Node> parents;
    ArrayList<Node> children;
    int color;

    int subTreeSize;

	Node(int value) {
		id = value;
        parents =  new ArrayList<Node>();
		children = new ArrayList<Node>();
        color = 0;
	}

    public void addChildren(Node... nodes) {
      for (Node node : nodes) {
        children.add(node);
      }
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setSubTreeSize(int size) {
        this.subTreeSize = size;
    }

    public int getId() {
        return this.id;
    }

    public ArrayList<Node> getParents() {
        return this.parents;
    }

    public ArrayList<Node> getChildren() {
        return this.children;
    }

    public int getSubTreeSize() {
        return this.subTreeSize;
    }

    public int getColor() {
        return this.color;
    }


}