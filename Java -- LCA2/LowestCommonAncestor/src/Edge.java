
public class Edge {
    int from;
    int to;
    public Edge(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getFrom(){
        return this.from;
    }

    public int getTo(){
        return this.to;
    }
}
