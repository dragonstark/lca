import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class DAGTest {

    @Test
    public void testDirectedAcrylicGraphLCA() {
    //     // Create Tree Same as wikipedia Page, see https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Tred-G.svg/330px-Tred-G.svg.png
    //     /*
    //     *             a(0)
    //     *     ___/   /    \  \__
    //     *    /      /     |     \
    //     *  b(1)     |    c(2)    | 
    //     *    \___   |   / |      |
    //     *        \  |  /  |     /
    //     *         d(3)    |    /
    //     *          \_____ | __/
    //     *                \|/     
    //     *                e(4)
    //     */

        LCA tree = new LCA();
		for(int i = 0; i < 5; i++){
			tree.addNode(i);
		}
		tree.addEdge(0, 1);
		tree.addEdge(0, 2);
		tree.addEdge(0, 3);
		tree.addEdge(0, 4);
		tree.addEdge(1, 3);
		tree.addEdge(2, 3);
		tree.addEdge(2, 4);
		tree.addEdge(3, 4);

        ArrayList<Integer> predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Find lowest common ancestor 0 and 0", predictedResult, tree.findLCA(0, 0));
        assertEquals("Find lowest common ancestor 0 and 1", predictedResult, tree.findLCA(0, 1));
        assertEquals("Find lowest common ancestor 0 and 2", predictedResult, tree.findLCA(0, 2));
        assertEquals("Find lowest common ancestor 0 and 3", predictedResult, tree.findLCA(0, 3));
        assertEquals("Find lowest common ancestor 0 and 4", predictedResult, tree.findLCA(0, 4));

        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Find lowest common ancestor 1 and 0", predictedResult, tree.findLCA(1, 0));
        predictedResult = new ArrayList<>();
        predictedResult.add(1);
        assertEquals("Find lowest common ancestor 1 and 1", predictedResult, tree.findLCA(1, 1));
        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Find lowest common ancestor 1 and 2", predictedResult, tree.findLCA(1, 2));
        predictedResult = new ArrayList<>();
        predictedResult.add(1);
        assertEquals("Find lowest common ancestor 1 and 3", predictedResult, tree.findLCA(1, 3));
        assertEquals("Find lowest common ancestor 1 and 4", predictedResult, tree.findLCA(1, 4));

        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Find lowest common ancestor 2 and 0", predictedResult, tree.findLCA(2, 0));
        assertEquals("Find lowest common ancestor 2 and 1", predictedResult, tree.findLCA(2, 1));
        predictedResult = new ArrayList<>();
        predictedResult.add(2);
        assertEquals("Find lowest common ancestor 2 and 2", predictedResult, tree.findLCA(2, 2));
        assertEquals("Find lowest common ancestor 2 and 3", predictedResult, tree.findLCA(2, 3));
        assertEquals("Find lowest common ancestor 2 and 4", predictedResult, tree.findLCA(2, 4));

        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Find lowest common ancestor 3 and 0", predictedResult, tree.findLCA(3, 0));
        predictedResult = new ArrayList<>();
        predictedResult.add(1);
        assertEquals("Find lowest common ancestor 3 and 1", predictedResult, tree.findLCA(3, 1));
        predictedResult = new ArrayList<>();
        predictedResult.add(2);
        assertEquals("Find lowest common ancestor 3 and 2", predictedResult, tree.findLCA(3, 2));
        predictedResult = new ArrayList<>();
        predictedResult.add(3);
        assertEquals("Find lowest common ancestor 3 and 3", predictedResult, tree.findLCA(3, 3));
        assertEquals("Find lowest common ancestor 3 and 4", predictedResult, tree.findLCA(3, 4));

        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Find lowest common ancestor 4 and 0", predictedResult, tree.findLCA(4, 0));
        predictedResult = new ArrayList<>();
        predictedResult.add(1);
        assertEquals("Find lowest common ancestor 4 and 1", predictedResult, tree.findLCA(4, 1));
        predictedResult = new ArrayList<>();
        predictedResult.add(2);
        assertEquals("Find lowest common ancestor 4 and 2", predictedResult, tree.findLCA(4, 2));
        predictedResult = new ArrayList<>();
        predictedResult.add(3);
        assertEquals("Find lowest common ancestor 4 and 3", predictedResult, tree.findLCA(4, 3));
        predictedResult = new ArrayList<>();
        predictedResult.add(4);
        assertEquals("Find lowest common ancestor 4 and 4", predictedResult, tree.findLCA(4, 4));

    }

        @Test
    public void testDirectedAcrylicGraphWithMultipleLCAs() {
    //     /*
    //     *             a(0)
    //     *     ___/       \  
    //     *    /           |      
    //     *  b(1)         c(2)     
    //     *    | \ _____/ /       
    //     *    | /\____  /    
    //     *   d(3)      e(4) 
    //     *              |
    //     *              |       
    //     *             f(5)
    //     */

        LCA tree = new LCA();
		for(int i = 0; i < 6; i++){
			tree.addNode(i);
		}
		tree.addEdge(0, 1);
		tree.addEdge(0, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(2, 3);
		tree.addEdge(2, 4);
        tree.addEdge(4, 5);


        ArrayList<Integer> predictedResult = new ArrayList<>();
        predictedResult.add(1);
        predictedResult.add(2);
        ArrayList<Integer> actualResult = tree.findLCA(3, 5);
        assertTrue("Find lowest common ancestor 3 and 5", predictedResult.size() == actualResult.size() && predictedResult.containsAll(actualResult) && actualResult.containsAll(predictedResult));


    }
}
