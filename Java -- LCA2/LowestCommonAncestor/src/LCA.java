import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LCA
{

	Node root;
	public Set<Integer> nodeOnePaths = new HashSet<Integer>();
	public Set<Integer> nodeTwoPaths = new HashSet<Integer>();
	private ArrayList<Node> Nodes = new ArrayList<>();
	private ArrayList<Edge> Edges = new ArrayList<>();

	
	// Finds the path from root node to given root of the tree.
	ArrayList<Integer> findLCA(int n1, int n2) {
		nodeOnePaths.clear();
		nodeTwoPaths.clear();
		if(Nodes.size() > 0 && checkTreeContainsId(n1) && checkTreeContainsId(n2)) {
			root = Nodes.get(0);
			return findLCAInternal(root, n1, n2);
		}
		return new ArrayList<Integer>();
	}

	private ArrayList<Integer> findLCAInternal(Node root, int n1, int n2) {
		
		ArrayList<Integer> answers = new ArrayList<>();
		dfs(root, n1, 1);
		dfs(root, n2, 2); 

		Set<Integer> commonIds = new HashSet<>();

		for(Integer a : nodeOnePaths){
			if(nodeTwoPaths.contains(a))
				commonIds.add(a);
		}

		Set<Node> commonNodes = new HashSet<>();

		for(Node node: Nodes) {
			for(Integer a : commonIds){
				if(node.getId() == a){
					commonNodes.add(node);
				}
			}
		}

		for(Node node : commonNodes) {
			boolean isOut = false;
			for(Node child : node.getChildren()){
				if(commonIds.contains(child.getId())) {
					isOut = true;
				}
			}
			if(!isOut) {
				answers.add(node.getId());
			}

		}

		return answers;
	}

	   public static <T> Set<T> mergeSet(Set<T> a, Set<T> b)
    {
        return new HashSet<T>() {{
                      addAll(a);
                       addAll(b);
        } };
    }
	

	public void dfs(Node start, int target, int nDfs) {
		boolean[] isVisited = new boolean[Nodes.size()];
		ArrayList<Integer> nodesPath = new ArrayList<>();
		nodesPath.add(start.getId());
		dfsRecursive(start.getId(), target, isVisited, nDfs, nodesPath);
	}

	private void dfsRecursive(int s, int target, boolean[] isVisited, int nDfs, List<Integer> nodesPath) {

		if(s == target) {
			addPath(nodesPath, nDfs);
		}
		int[] arr = new int[Nodes.get(s).getChildren().size()];
		for(int i = 0; i < arr.length; i++) {
			arr[i] = Nodes.get(s).getChildren().get(i).getId();
		}
		for (Integer i : arr) {
			if (!isVisited[i]) {
				nodesPath.add(i);
				dfsRecursive(i, target, isVisited, nDfs, nodesPath);
				nodesPath.remove(i);
			}
		}
		isVisited[Nodes.get(s).getId()] = false;
	}

	public void addPath(List<Integer> list, int path){
		Integer[] arr = new Integer[list.size()];
		int j = 0;
		for(Integer a : list) {
			arr[j] = a;
			j++;
		}
		if(path == 1) {
			for(int i = 0; i < arr.length; i++){
				nodeOnePaths.add(arr[i]);
			}
		} else if(path == 2) {
			for(int i = 0; i < arr.length; i++){
				nodeTwoPaths.add(arr[i]);
			}
		}

	}

	public boolean checkTreeContainsId(Integer n) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for(Node node: Nodes) {
			ids.add(node.getId());
		}
		return ids.contains(n);
	}

	public void addNode(int id) {
		Nodes.add(new Node(id));
	}

	public void addEdge(int from, int to){
		Edges.add(new Edge(from, to));
		for(int i = 0; i < Nodes.size(); i++) {
			if(Nodes.get(i).getId() == to) {
				Nodes.get(from).children.add(Nodes.get(i));
			}
		}
	}


	// Driver code
	public static void main(String[] args)
	{

		LCA tree = new LCA();
		for(int i = 0; i < 9; i++){
			tree.addNode(i);
		}
		tree.addEdge(0, 1);
		tree.addEdge(0, 2);
		tree.addEdge(1, 4);
		tree.addEdge(1, 6);
		tree.addEdge(2, 4);
		tree.addEdge(2, 6);
		tree.addEdge(2, 3);
		tree.addEdge(3, 6);
		tree.addEdge(6, 5);
		tree.addEdge(6, 7);
		tree.addEdge(7, 8);

		System.out.print("\nLCA(7, 4): ");
		ArrayList<Integer> lcas = new ArrayList<>();
		lcas = tree.findLCA(7,4);
		for(Integer ans: lcas) {
			System.out.print(ans + " ");
		}
	
	}
}