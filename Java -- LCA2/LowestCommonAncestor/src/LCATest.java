import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class LCATest {

    @Test
    public void testEmptyTreeLCA() {
        LCA tree = new LCA();
        ArrayList<Integer> predictedResult = new ArrayList<>();
        assertEquals("Find lowest common ancestor of Empty Tree", predictedResult, tree.findLCA(1, 2));
    }

    @Test
    public void testMissingNodeLCA() {
		LCA tree = new LCA();
		for(int i = 1; i < 2; i++){
			tree.addNode(i);
		}
        ArrayList<Integer> predictedResult = new ArrayList<>();
        assertEquals("Test when Node in function arguments does not exist", predictedResult, tree.findLCA(1, 2));
    }

    @Test
    public void testSameNodeLCA() {
		LCA tree = new LCA();
		for(int i = 0; i < 1; i++){
			tree.addNode(i);
		}
        ArrayList<Integer> predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Test when same node is put in argument twice", predictedResult, tree.findLCA(0, 0));
    }

    @Test
    public void testBasicLCA() {
        LCA tree = new LCA();
		for(int i = 0; i < 3; i++){
			tree.addNode(i);
		}
		tree.addEdge(0, 1);
		tree.addEdge(0, 2);

        /*
        *       0
        *     /   \
        *    1     2
        */
        ArrayList<Integer> predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Test Basic Tree with 3 nodes: Check LCA of leaves", predictedResult, tree.findLCA(1,2));
        assertEquals("Test Basic Tree with 3 nodes: Check LCA of root and left leaf.", predictedResult, tree.findLCA(0,1));
        assertEquals("Test Basic Tree with 3 nodes: Check LCA of root and right leaf.", predictedResult, tree.findLCA(0,2));

        assertEquals("Test Basic Tree with 3 nodes: Check LCA of root and root.", predictedResult, tree.findLCA(0,0));
        predictedResult = new ArrayList<>();
        predictedResult.add(1);
        assertEquals("Test Basic Tree with 3 nodes: Check LCA of left leaf and left leaf.", predictedResult, tree.findLCA(1,1));
        predictedResult = new ArrayList<>();
        predictedResult.add(2);
        assertEquals("Test Basic Tree with 3 nodes: Check LCA of right leaf and right leaf.", predictedResult, tree.findLCA(2,2));
    }

    @Test
    public void testWithMoreComplexTree() {
        LCA tree = new LCA();
		for(int i = 0; i < 7; i++){
			tree.addNode(i);
		}
		tree.addEdge(0, 1);
		tree.addEdge(0, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(2, 5);
		tree.addEdge(2, 6);

        /*
        *       0
        *     /   \
        *    1     2
        *   / \   / \
        *  3   4 5   6
        */

        ArrayList<Integer> predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Check LCA of root and root.", predictedResult, tree.findLCA(0,0));
        predictedResult = new ArrayList<>();
        predictedResult.add(1);
        assertEquals("Check LCA of left leaf(DEPTH 3) and left leaf(DEPTH 2).", predictedResult, tree.findLCA(1,1));
        predictedResult = new ArrayList<>();
        predictedResult.add(2);
        assertEquals("Check LCA of right leaf(DEPTH 2) and right leaf(DEPTH 2).", predictedResult, tree.findLCA(2,2));
        predictedResult = new ArrayList<>();
        predictedResult.add(3);
        assertEquals("Check LCA of left left leaf(DEPTH 2) and left left leaf(DEPTH 2).", predictedResult, tree.findLCA(3,3));
        predictedResult = new ArrayList<>();
        predictedResult.add(4);
        assertEquals("Check LCA of left right leaf(DEPTH 3) and left right leaf(DEPTH 3).", predictedResult, tree.findLCA(4,4));
        predictedResult = new ArrayList<>();
        predictedResult.add(5);
        assertEquals("Check LCA of right left leaf(DEPTH 3) and right left leaf(DEPTH 2).", predictedResult, tree.findLCA(5,5));
        predictedResult = new ArrayList<>();
        predictedResult.add(6);
        assertEquals("Check LCA of right right leaf(DEPTH 3) and right right leaf(DEPTH 2).", predictedResult, tree.findLCA(6,6));

        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Check LCA of root and left leaf(DEPTH 2).", predictedResult, tree.findLCA(0,1));
        assertEquals("Check LCA of root and right leaf(DEPTH 2).", predictedResult, tree.findLCA(0,2));
        assertEquals("Check LCA of left and right leaves(DEPTH 2)", predictedResult, tree.findLCA(1,2));
        assertEquals("Check LCA of root and left left leaf(DEPTH 3).", predictedResult, tree.findLCA(0,3));
        assertEquals("Check LCA of root and left right leaf(DEPTH 3).", predictedResult, tree.findLCA(0,4));
        assertEquals("Check LCA of root and right left leaf(DEPTH 3).", predictedResult, tree.findLCA(0,5));
        assertEquals("Check LCA of root and right right leaf(DEPTH 3).", predictedResult, tree.findLCA(0,6));
   
        predictedResult = new ArrayList<>();
        predictedResult.add(1);
        assertEquals("Check LCA of left(DEPTH 2) and left left leaf(DEPTH 3).", predictedResult, tree.findLCA(1,3));
        assertEquals("Check LCA of left(DEPTH 2) and left right leaf(DEPTH 3).", predictedResult, tree.findLCA(1,4));
        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Check LCA of left(DEPTH 2) and right left leaf(DEPTH 3).", predictedResult, tree.findLCA(1,5));
        assertEquals("Check LCA of left(DEPTH 2) and right right leaf(DEPTH 3).", predictedResult, tree.findLCA(1,6));

        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Check LCA of right(DEPTH 2) and left left leaf(DEPTH 3).", predictedResult, tree.findLCA(2,3));
        assertEquals("Check LCA of right(DEPTH 2) and left right leaf(DEPTH 3).", predictedResult, tree.findLCA(2,4));
        predictedResult = new ArrayList<>();
        predictedResult.add(2);
        assertEquals("Check LCA of right(DEPTH 2) and right left leaf(DEPTH 3).", predictedResult, tree.findLCA(2,5));
        assertEquals("Check LCA of right(DEPTH 2) and right right leaf(DEPTH 3).", predictedResult, tree.findLCA(2,6));

        predictedResult = new ArrayList<>();
        predictedResult.add(1);
        assertEquals("Check LCA of left left(DEPTH 3) and left right leaf(DEPTH 3).", predictedResult, tree.findLCA(3,4));
        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Check LCA of left left(DEPTH 3) and right left leaf(DEPTH 3).", predictedResult, tree.findLCA(3,5));
        assertEquals("Check LCA of left left(DEPTH 3) and right right leaf(DEPTH 3).", predictedResult, tree.findLCA(3,6));

        predictedResult = new ArrayList<>();
        predictedResult.add(0);
        assertEquals("Check LCA of left right(DEPTH 3) and right left leaf(DEPTH 3).", predictedResult, tree.findLCA(4,5));
        assertEquals("Check LCA of left right(DEPTH 3) and right right leaf(DEPTH 3).", predictedResult, tree.findLCA(4,6));

        predictedResult = new ArrayList<>();
        predictedResult.add(2);
        assertEquals("Check LCA of right left(DEPTH 3) and right right leaf(DEPTH 3).", predictedResult, tree.findLCA(5,6));
    }
}
