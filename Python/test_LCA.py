import unittest
from LCA import findLCA
from LCA import Node

class LCATest(unittest.TestCase):

    def test_missing_node_tree(self):
        # Assume
        root = Node(1)
        root = Node(2)

        # Action
        result = findLCA(root, 2, 3)

        # Assert
        self.assertEqual(-1, result, "Test when looking for shortest path with a node that does'nt exist.")

    def test_same_node(self):
        # Assume
        root = Node(1)

        # Action
        result = findLCA(root, 1, 1)

        # Assert
        self.assertEqual(1, result, "Test when both nodes are the same")   

    def test_basic_tree(self):
        # Assume
        root = Node(1)
        root.left = Node(2)
        root.right = Node(3)

        #
        #       1
        #     /   \
        #    2     3
        #

        # Action
        result = findLCA(root, 2, 3)
        # Assert
        self.assertEqual(1, result, "Check LCA of leaves") 
        # Action
        result = findLCA(root, 1, 2)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and left leaf.")  
        # Action
        result = findLCA(root, 1, 3)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and right leaf.")   

        # Action
        result = findLCA(root, 1, 1)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and root.") 
        # Action
        result = findLCA(root, 2, 2)
        # Assert
        self.assertEqual(2, result, "Check LCA of left leaf and left leaf.")   
        # Action
        result = findLCA(root, 3, 3)
        # Assert
        self.assertEqual(3, result, "Check LCA of right leaf and right leaf.")  

    def test_with_more_complex_tree(self):
                # Assume
        root = Node(1)
        root.left = Node(2)
        root.right = Node(3)
        root.left.left = Node(4)
        root.left.right = Node(5)
        root.right.left = Node(6)
        root.right.right = Node(7)

        #
        #       1
        #     /   \
        #    2     3
        #   / \   / \
        #  4   5 6   7

                # Action
        result = findLCA(root, 1, 1)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and root.") 
        # Action
        result = findLCA(root, 2, 2)
        # Assert
        self.assertEqual(2, result, "Check LCA of left leaf(DEPTH 2) and left leaf(DEPTH 2).")   
        # Action
        result = findLCA(root, 3, 3)
        # Assert
        self.assertEqual(3, result, "Check LCA of right leaf(DEPTH 2) and right leaf(DEPTH 2).")  
        # Action
        result = findLCA(root, 4, 4)
        # Assert
        self.assertEqual(4, result, "Check LCA of left left leaf(DEPTH 3) and left left leaf(DEPTH 3)")  
        # Action
        result = findLCA(root, 5, 5)
        # Assert
        self.assertEqual(5, result, "Check LCA of left right leaf(DEPTH 3) and left right leaf(DEPTH 2).")  
        # Action
        result = findLCA(root, 6, 6)
        # Assert
        self.assertEqual(6, result, "Check LCA of right left leaf(DEPTH 3) and right left leaf(DEPTH 2).")  
        # Action
        result = findLCA(root, 7, 7)
        # Assert
        self.assertEqual(7, result, "Check LCA of right right leaf(DEPTH 3) and right right leaf(DEPTH 2).")  

        # Action
        result = findLCA(root, 1, 2)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and left leaf(DEPTH 2).")  
        # Action
        result = findLCA(root, 1, 3)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and right leaf(DEPTH 2).")    
        # Action
        result = findLCA(root, 2, 3)
        # Assert
        self.assertEqual(1, result, "Check LCA of leaves(DEPTH 2).") 

        # Action
        result = findLCA(root, 1, 4)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and left left leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 1, 5)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and left right leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 1, 6)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and right left leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 1, 7)
        # Assert
        self.assertEqual(1, result, "Check LCA of root and right right leaf(DEPTH 3).") 

        # Action
        result = findLCA(root, 2, 4)
        # Assert
        self.assertEqual(2, result, "Check LCA of left(DEPTH 2) and left left leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 2, 5)
        # Assert
        self.assertEqual(2, result, "Check LCA of left(DEPTH 2) and left right leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 2, 6)
        # Assert
        self.assertEqual(1, result, "Check LCA of left(DEPTH 2) and right left leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 2, 7)
        # Assert
        self.assertEqual(1, result, "Check LCA of left(DEPTH 2) and right right leaf(DEPTH 3).") 

        # Action
        result = findLCA(root, 3, 4)
        # Assert
        self.assertEqual(1, result, "Check LCA of right(DEPTH 2) and left left leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 3, 5)
        # Assert
        self.assertEqual(1, result, "Check LCA of right(DEPTH 2) and left right leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 3, 6)
        # Assert
        self.assertEqual(3, result, "Check LCA of right(DEPTH 2) and right left leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 3, 7)
        # Assert
        self.assertEqual(3, result, "Check LCA of right(DEPTH 2) and right right leaf(DEPTH 3).") 

        # Action
        result = findLCA(root, 4, 5)
        # Assert
        self.assertEqual(2, result, "Check LCA of left left(DEPTH 3) and left right leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 4, 6)
        # Assert
        self.assertEqual(1, result, "Check LCA of left left(DEPTH 3) and right left leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 4, 7)
        # Assert
        self.assertEqual(1, result, "Check LCA of left left(DEPTH 3) and right right leaf(DEPTH 3).") 

        # Action
        result = findLCA(root, 5, 6)
        # Assert
        self.assertEqual(1, result, "Check LCA of left right(DEPTH 3) and right left leaf(DEPTH 3).") 
        # Action
        result = findLCA(root, 5, 7)
        # Assert
        self.assertEqual(1, result, "Check LCA of left right(DEPTH 3) and right right leaf(DEPTH 3).") 

        # Action
        result = findLCA(root, 6, 7)
        # Assert
        self.assertEqual(3, result, "Check LCA of right left(DEPTH 3) and right right leaf(DEPTH 3).") 
          